layui.define(['jquery','layer'], function(exports){


  var $ = layui.$;
  var layer = layui.layer;

  //layer open 支持5种层类型。可传入的值有：0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）。
  $("button[lay-filter='open1']").on("click",function (e){
    layer.open({
      type: 2,//type 2
      shade:0.3,//弹层外区域。默认是0.3透明度的黑色背景（'#000'）,如果你想定义别的颜色，可以shade: [0.8, '#393D49']；如果你不想显示遮罩，可以shade: 0。 设成0，弹层外区域就可以点击了。
      title: "title",
      content: 'http://sentsin.com',
      area: ["512px", "512px"],
    });
  })


  $("button[lay-filter='open2']").on("click",function (e){
    layer.alert('只想简单的提示');//type 0
  })

  $("button[lay-filter='open3']").on("click",function (e){
    layer.confirm('is not?', {icon: 3, title:'提示'})
  })

  $("button[lay-filter='open4']").on("click",function (e){
    //msg不是模态效果的，其他的需要设置shade为0才不是模态的
    layer.msg('同上', {
      icon: 2,
      time: 2000, //2秒关闭（如果不配置，默认是3秒）
      offset: "15px" //top
    })
  })

  $("button[lay-filter='open5']").on("click",function (e){
    var index = layer.load(0, {time: 5*1000}); //type 3
    //关闭
    setTimeout(function(){
      layer.close(index);
    },2000);
  })


  exports('layer', {});
});
