layui.define('upload', function(exports){
  var $ = layui.jquery
  var upload = layui.upload;

  //执行实例
  var uploadInst = upload.render({
    elem: '#test1' //绑定元素
    ,url: '/upload/' //上传接口
    ,auto: false //选择文件后不自动上传
    ,size: 5000 //限制文件大小，单位 KB
    ,choose: function(obj){
      //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
      obj.preview(function(index, file, result){
        console.log(index); //得到文件索引
        console.log(file); //得到文件对象
        console.log(result); //得到文件base64编码，比如图片
        $('#demo1').attr('src', result)
        obj.upload(index, file)
      })
    }
    ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
      layer.load(); //上传loading
    }
    ,done: function(res){
      layer.closeAll('loading');
      //上传完毕回调
      if(res.code == 0){
        return layer.msg('上传失败');
      }
    }
    ,error: function(){
      layer.closeAll('loading');
      //请求异常回调
    }
  });

  exports('upload',{})
});
