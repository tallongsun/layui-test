layui.extend({
  "layarea":"{/}mod/layarea",
}).define(['jquery','form','laydate','layarea'], function(exports){
  var form = layui.form;
  var laydate = layui.laydate;
  var layarea = layui.layarea

  laydate.render({
    elem: '#test1' //指定元素
    ,type: 'datetime'
    ,range: true //或 range: '~' 来自定义分割字符
    ,format: 'yyyy-MM-dd HH:mm:ss'
    ,lang: 'en'
    ,theme: 'grid'
    ,done: function(value, date, endDate){
      console.log(value); //得到日期生成的值，如：2017-08-18
      console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
      console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
    }
  });


  layarea.render({
      elem: '#area-picker',
      change: function (res) {
          //选择结果
          console.log(res);
      }
  });

  form.on('select(aihao)',function(data){
    console.log(data.elem); //得到select原始DOM对象
    console.log(data.value); //得到被选中的值
    console.log(data.othis); //得到美化后的DOM对象
  })

  form.on('submit(*)', function(data){
    console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
    console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
    console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
    return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
  });

  form.verify({
    username: function(value, item){ //value：表单的值、item：表单的DOM对象
      if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
        return '用户名不能有特殊字符';
      }
      if(/(^\_)|(\__)|(\_+$)/.test(value)){
        return '用户名首尾不能出现下划线\'_\'';
      }
      if(/^\d+\d+\d$/.test(value)){
        return '用户名不能全为数字';
      }
    }

    //我们既支持上述函数式的方式，也支持下述数组的形式
    //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
    ,pass: [
      /^[\S]{6,12}$/
      ,'密码必须6到12位，且不能出现空格'
    ]
  });

  form.val("example", {
    "username": "贤心" // "name": "value"
  })

  exports('form', {});
});
