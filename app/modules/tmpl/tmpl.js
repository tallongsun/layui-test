layui.define(['laytpl','jquery'], function(exports){
  var $ = layui.jquery;
  var laytpl = layui.laytpl;

  laytpl('{{ d.name }}是一位公猿').render({
     name: '贤心'
   }, function(string){
     console.log(string); //贤心是一位公猿
   });

  var data = { //数据
    "title":"Layui常用模块"
    ,"list":[{"modname":"弹层","alias":"layer","site":"layer.layui.com"},{"modname":"表单","alias":"form"}]
  }
  var tpl = $('#demo').html()
  var view = $('#view');

  laytpl(tpl).render(data, function(html){
    view.html(html);
  });

  exports('tmpl', {});
});
